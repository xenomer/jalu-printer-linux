# Järvenpää Lukio printer drivers for linux

This repository contains the drivers (and the documentation) to install printer drivers in Linux.
The installation requires some technological knowledge about your distro as I don't know
everything ¯\\\_(ツ)\_/¯

Got bored on chemistry lesson and so I had the idea to try to reverse-engineer the Windows
script for installing the drivers. Using the printer model string and ip-address found in the
.bat file, I was able to find the drivers from the Internet (there are drivers specifically for
Linux lol) and add them manually.



Credits go for me (Johannes Vääräkangas) for finding and writing this documentation.



## How to install

### 1. Download the repo

If you don't have it already, you gotta download the repo (or the `drivers` folder)
as it contains the actual drivers for the printer.

### 2. Add a network printer

This part differs between distros/WMs, but on GNOME, it's in
`settings->devices->printers->add...`.  The IP address for the printer is `172.16.5.41`.

### 3. Install the drivers

In GNOME, click on printer's gear button, `Printer details`, `Install PPD file...`

Install the file from `drivers/English/CUPS1.2/` and try both of the files and which one
works for you.

### 4. Print a test page

If successful, it seems that it's working! If not, try again or something.

### 5. Profit

Nice!